package com.pnuema.android.locationapp.api

import com.pnuema.android.locationapp.mainscreen.ui.models.LocationResult

interface LocationResultsListener {
    fun success(locations: ArrayList<LocationResult>)
    fun failed()
}