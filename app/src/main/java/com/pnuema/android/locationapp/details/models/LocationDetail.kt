package com.pnuema.android.locationapp.details.models

class LocationDetail {
    var lat: Double? = null
    var lng: Double? = null
    var formattedAddress: List<String> = ArrayList()
}