package com.pnuema.android.locationapp.mainscreen.models

class Category {

    var id: String? = null
    var name: String? = null
    var pluralName: String? = null
    var icon: Icon? = null

}
