package com.pnuema.android.locationapp.mainscreen.ui.viewholders

interface LocationClickListener {
    fun onLocationClicked(id: String)
    fun onFavoriteClicked(id: String)
}