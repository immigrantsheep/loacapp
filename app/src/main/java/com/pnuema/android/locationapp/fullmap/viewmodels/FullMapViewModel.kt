package com.pnuema.android.locationapp.fullmap.viewmodels

import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.pnuema.android.locationapp.mainscreen.ui.models.LocationResult

class FullMapViewModel : ViewModel() {
    var locationResults: List<LocationResult> = ArrayList()
    var currentLocation: LatLng? = null
}